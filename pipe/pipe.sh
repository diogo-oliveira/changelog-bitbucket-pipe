#!/usr/bin/env bash
#
# Este pipe gera changelog do repositório 
#

source "$(dirname "$0")/common.sh"

info "Iniciando Changelog Bitbucket..."

# PARAMETROS OBRIGATORIOS
BITBUCKET_USERNAME=${BITBUCKET_USERNAME:="$BITBUCKET_REPO_OWNER"}
debug BITBUCKET-USERNAME: "${BITBUCKET_USERNAME}"

BITBUCKET_APP_PASSWORD=${BITBUCKET_APP_PASSWORD:?'não informado.'}
debug BITBUCKET-APP-PASSWORD: "${BITBUCKET_APP_PASSWORD}"

BITBUCKET_REPOSITORY=${BITBUCKET_REPOSITORY:="$BITBUCKET_REPO_FULL_NAME"}
debug BITBUCKET-REPOSITORY: "${BITBUCKET_REPOSITORY}"

CHANGELOG_DIR=${CHANGELOG_DIR:=$BITBUCKET_PIPE_SHARED_STORAGE_DIR}
debug CHANGELOG-DIR: "$CHANGELOG_DIR"

CHANGELOG_FILE="${CHANGELOG_DIR}/changelog.txt"
debug CHANGELOG-FILE: "$CHANGELOG_FILE"

BRANCH_PRODUCTION=${BRANCH_PRODUCTION:="master"}
debug BRANCH-PRODUCTION: "${BRANCH_PRODUCTION}"

BRANCH_DEVELOPMENT=${BRANCH_DEVELOPMENT:="develop"}
debug BRANCH-DEVELOPMENT: "${BRANCH_DEVELOPMENT}"

BY_TAG=${TYPE:=false}
debug TYPE: "${TYPE}"

RELEASE=${RELEASE:=true}
debug RELEASE: "${RELEASE}"

SRC_DIR=/tmp/src
mkdir $SRC_DIR

if [ "${BY_TAG}" = true ]; then
  RELEASE=true
fi

if [ "${RELEASE}" = true ]; then
  BRANCH_CLONE=$BRANCH_PRODUCTION
else
  BRANCH_CLONE=$BRANCH_DEVELOPMENT
fi

cd $SRC_DIR

run git clone --branch="$BRANCH_CLONE" \
      https://$BITBUCKET_USERNAME:$BITBUCKET_APP_PASSWORD@bitbucket.org/$BITBUCKET_REPOSITORY.git $SRC_DIR

if [ "${RELEASE}" = true ]; then
  TAGS_COUNT=$(git rev-list `git rev-list --tags --no-walk --max-count=1`..HEAD --count)

  if [ "${TAGS_COUNT}" -gt 0 ]; then 
  
    if [ "${BY_TAG}" = true ]; then
        LOG_ARGS='$(git describe --abbrev=0 --tags `git rev-list --tags --skip=1 --max-count=1`)..\
                  $(git describe --tags `git rev-list --tags --max-count=1`)'
      else
        LOG_ARGS='`git describe --abbrev=0 --tags`..HEAD'
    fi

  else
    LOG_ARGS=''
  fi
else 
  run git fetch origin $BRANCH_PRODUCTION:$BRANCH_PRODUCTION
  LOG_ARGS="$BRANCH_DEVELOPMENT ^$BRANCH_PRODUCTION"
fi

info "gerando \"changelog.txt\" arquivo..."

eval "git log \
        --no-merges \
        --format='• %s' $LOG_ARGS > $CHANGELOG_FILE"

debug CHANGELOG: "↩\n$(cat $CHANGELOG_FILE)"

if [[ "${status}" == "0" ]]; then
  success "Changelog Bitbucket executado com sucesso!"
else
  fail "Changelog Bitbucket falhou! :("
fi
