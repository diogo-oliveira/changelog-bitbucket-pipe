#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/changelog-bitbucket-pipe"}

  # echo "Building image..."
  # docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
    run docker run --rm \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    run docker build -t ${DOCKER_IMAGE}:test .

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

