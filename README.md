# Bitbucket Pipelines Pipe: Changelog Bitbucket

Este pipe gera changelog do repositório

## Definição de YAML

Adicione o seguinte trecho à seção de script do seu arquivo `bitbucket-pipelines.yml`:

```yaml
script:
  - pipe: diogo-oliveira/changelog-bitbucket-pipe:1.0.0
    variables:
      # DEBUG: "<boolean>" # Optional
```
## Variables

| Variável Uso               | Descrição                                                                                                       |
| -------------------------- | --------------------------------------------------------------------------------------------------------------- |
| BITBUCKET_USERNAME         | Usuário do repositório. Padrão: `BITBUCKET_REPO_OWNER`                                                          |
| BITBUCKET_APP_PASSWORD (*) | [Senha do aplicativo Bitbucket](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) do usuário que será usado. É recomendável usar uma variável de repositório seguro                                                                 |
| BITBUCKET_REPOSITORY       | Nome do repositório (`<USER>/<REPO_NAME>`). Padrão: `BITBUCKET_REPO_FULL_NAME`                                  |
| CHANGELOG_DIR              | Diretório de saída do arquivo de log. Padrão: `BITBUCKET_PIPE_SHARED_STORAGE_DIR`                               |
| BRANCH_PRODUCTION          | Nome da branch de produção. Padrão: `master`                                                                    |
| BRANCH_DEVELOPMENT         | Nome da branch de desenvolvimento. Padrão: `develop`                                                            |
| RELEASE                    | Flag do tipo de log que será gerado. Padrão: `true`                                                             |
| BY_TAG                     | Flag indica que o log dever ser gerado a partir das tags. Essa opção prevalece sobre a opção `RELEASE` será `true` por padrão. Padrão: `false`                                                             |

_ (*) ​​= variável necessária._

## Notas
O arquivo gerado é nomeado `changelog.txt`. Esse é o arquivo de que deve ser procurado no diretório definido para a pipe.

Quando utilizado `release` o log será baseado até a ultima tag definida na branch de produção. Caso não exista tag todos os commits 
serão usados para gerar o log. No caso de não utilizar `release` (`RELEASE="false"`) o log será baseado na diferença entre a branch de
produção com a branch de desenvolvimento.

## Pré-requisitos

## Exemplos

Exemplo básico:

```yaml
script:
  - pipe: diogo-oliveira/changelog-bitbucket-pipe:1.0.0
```

Exemplo avançado:

```yaml
script:
  - pipe: diogo-oliveira/changelog-bitbucket-pipe:1.0.0
    variables:
      RELEASE: "false"
      DEBUG: "true"
```

## Apoio, suporte
Se você precisar de ajuda com este canal ou tiver um problema ou solicitação de recurso, informe-nos.
O tubo é mantido por diogo0liveira@hotmail.com.

Se você estiver relatando um problema, inclua:

- a versão do pipe
- logs e mensagens de erro relevantes
- Passos para reproduzir
